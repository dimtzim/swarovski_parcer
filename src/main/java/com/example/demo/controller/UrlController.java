package com.example.demo.controller;

import com.example.demo.domain.FormSearch;
import com.example.demo.entities.ExternalUrl;
import com.example.demo.entities.InternalUrl;
import com.example.demo.entities.Url;
import com.example.demo.repos.UrlExternalRepo;
import com.example.demo.repos.UrlInternalRepo;
import com.example.demo.repos.UrlRepository;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.jws.WebParam;
import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class UrlController {

    private final UrlRepository urlRepository;

    @Autowired
    public UrlController(UrlRepository urlRepository) {
        this.urlRepository = urlRepository;
    }

    @Autowired
    private UrlExternalRepo urlExternalRepo;

    @Autowired
    private UrlInternalRepo urlInternalRepo;

    private Elements links;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Url url, Model model){
//        model.addAttribute("urlString", new Url());
        return "index";
    }

    @PostMapping("/search")
    public String goSearch(@Valid @ModelAttribute("url") Url url, BindingResult result, Model model) {

        getLinks(url.getUrlName());
        Url url1 = new Url(url.getUrlName(), getTheExternalLinks(url.getUrlName()), getInternalUrlList(url.getUrlName()));
        urlRepository.save(url1);
        model.addAttribute("externalUrls", urlExternalRepo.findAllByUrlId(url1.getId()));
        model.addAttribute("internalLinks", urlInternalRepo.findAllByUrlId(url1.getId()));
        return "index";
    }

    private void getLinks(String url) {
        Document doc = null;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        links = doc.select("a[href]"); // a with href
    }

    private Set<ExternalUrl> getTheExternalLinks(String url){
        Set<ExternalUrl> externalUrls = new HashSet<>();

        String externalURL;
        for (Element element: links){
            externalURL = element.attr("href");
            if (!externalURL.startsWith(url) && externalURL.startsWith("http")) {
                externalUrls.add(new ExternalUrl(externalURL));
                System.out.println(externalURL);
            }
        }
        return externalUrls;

    }

    private Set<InternalUrl> getInternalUrlList(String url) {
        Set<InternalUrl> internalUrls = new HashSet<>();

        String internalURL;
        for (Element element: links){
            internalURL = element.attr("href");
            if (internalURL.startsWith(url) || internalURL.startsWith("/")) {
                internalUrls.add(new InternalUrl(internalURL));
                System.out.println(internalURL);
            }
        }
        return internalUrls;
    }

}
