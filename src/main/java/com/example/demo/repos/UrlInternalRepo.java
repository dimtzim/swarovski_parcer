package com.example.demo.repos;

import com.example.demo.entities.InternalUrl;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UrlInternalRepo extends CrudRepository<InternalUrl, Long> {

    @Query(value = "SELECT * FROM internal_url internal where internal.url_id=:urlId", nativeQuery = true)
    List<InternalUrl> findAllByUrlId(@Param("urlId") int urlId);
}
