package com.example.demo.repos;

import com.example.demo.entities.ExternalUrl;
import com.example.demo.entities.InternalUrl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UrlExternalRepo extends JpaRepository<ExternalUrl, Integer> {

    @Query(value = "SELECT * FROM external_url ext where ext.url_id=:urlId", nativeQuery = true)
    List<ExternalUrl> findAllByUrlId(@Param("urlId") int urlId);
}
