package com.example.demo.entities;

import javax.persistence.*;

@Entity
@Table(name = "INTERNAL_URL")
public class InternalUrl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long internalId;

    @Column(name = "internal_url_name")
    private String internalUrlName;

    @ManyToOne
    @JoinColumn
    private Url url;

    public InternalUrl() {}

    public InternalUrl(String internalUrlName) {
        this.setInternalUrlName(internalUrlName);
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

    public String getInternalUrlName() {
        return internalUrlName;
    }

    public void setInternalUrlName(String internalUrlName) {
        this.internalUrlName = internalUrlName;
    }
}
