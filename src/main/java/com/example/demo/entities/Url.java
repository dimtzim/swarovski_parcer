package com.example.demo.entities;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@EqualsAndHashCode(exclude = "externalUrlList")

@Entity
public class Url {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Name is mandatory")
    private String urlName;

    @OneToMany(
            mappedBy = "url",
            cascade = CascadeType.ALL
    )
    private Set<ExternalUrl> externalUrlList;

    @OneToMany(fetch= FetchType.EAGER, cascade=CascadeType.ALL, mappedBy = "url")
    private Set<InternalUrl> internalUrlList;

    public Url() {}

    public Url(String url, Set<ExternalUrl> externalUrls, Set<InternalUrl> internalUrls) {

        this.setUrlName(url);
        
        this.externalUrlList = externalUrls;
        this.externalUrlList.forEach(x -> x.setUrl(this));
        
        this.internalUrlList = internalUrls;
        this.internalUrlList.forEach(x -> x.setUrl(this));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrlName() {
        return urlName;
    }

    public void setUrlName(String urlName) {
        this.urlName = urlName;
    }

    public Set<ExternalUrl> getExternalUrlList() {
        return externalUrlList;
    }

    public void setExternalUrlList(Set<ExternalUrl> externalUrlList) {
        this.externalUrlList = externalUrlList;
    }
}
