package com.example.demo.entities;

import lombok.Data;

import javax.persistence.*;

@Data

@Entity
@Table(name = "EXTERNAL_URL")
public class ExternalUrl {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int externalId;

    @Column(name = "external_url_name")
    private String externalUrlName;

    @ManyToOne
    @JoinColumn
    private Url url;

    public ExternalUrl() {}

    public ExternalUrl(String href) {
        this.setExternalUrlName(href);
    }

    public String getExternalUrlName() {
        return externalUrlName;
    }

    public void setExternalUrlName(String externalUrlName) {
        this.externalUrlName = externalUrlName;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }
}
