package com.example.demo;

import com.example.demo.repos.UrlExternalRepo;
import com.example.demo.repos.UrlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	private UrlExternalRepo urlExternalRepo;

	@Autowired
	private UrlRepository urlRepository;

	public static void main(String[] args) {


		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		urlExternalRepo.deleteAllInBatch();
		urlRepository.deleteAllInBatch();
	}

}
